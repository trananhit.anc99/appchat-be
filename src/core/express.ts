import express, { Express } from 'express';
import morgan from 'morgan';
import chalk, { Chalk } from 'chalk';
import {logger, morganOption} from '../../config/winston'
import { env } from '../../config/default';
import cors = require('cors');
// Import Chat Serve
import { ChatServe } from './chatServe'
export const ServerNode = async () => {
    const app: Express = express();
    const port = env.port || 8000
    const server = require('http').Server(app)
    // Serve Char
    await ChatServe(server);
    // Cors
    app.use(cors({
        origin: 'http://localhost:3000',
        methods: ['GET', 'POST', 'PUT', 'DELETE'],
        exposedHeaders: ['Content-Range', 'X-Content-Range'],
        allowedHeaders: ['Content-Type', 'Authorization'],
        maxAge: 100,
        credentials: true,
        optionsSuccessStatus: 20

    }))
    // Morgan
    app.use(morgan('tiny',morganOption));
    app.get('/', (req, res) => res.send('Hello World!'))
    server.listen(port, () => logger.info((chalk.bold.cyanBright.underline as Chalk)(`Port run ${port}`)))
}
