import {logger} from '../../config/winston'
import * as http from 'http'

export const ChatServe = (server: any) => {
  const io = require('socket.io')(server, {
    origins: '*:*'
  })
  io.set('origins', '*:*');
  io.on('connection',  (socket:any) => {
    logger.info('Client connected')
      socket.emit('news', { hello: 'world' });
      socket.broadcast.emit('messages', 'User has joi conversation ');

      socket.on('chatMessages', (data: any) => {
          logger.info(data.msg);
      });
      socket.on('disconnect', () => {
        io.emit('user disconnected');
      });
  });
}