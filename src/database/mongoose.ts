import mongoose from 'mongoose';
import {logger} from '../../config/winston'
type URL = {
    db: any
}

export const DB =  ({db}: URL) => {
    const connect = () => {
        mongoose
            .connect(
                db,
                {useNewUrlParser: true, useUnifiedTopology: true}
            )
            .then(() => logger.info(`DB Connected URL success`))
            .catch((err) => logger.error(`DB connect err: ${err}`))
    }
    connect();
    mongoose.connection.on('disconnected', connect)
}

