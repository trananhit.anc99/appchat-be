import appRoot from 'app-root-path';
import winston from 'winston';
import { Options } from 'morgan';


const options = {
    file: {
      level: 'info',
      filename: `${appRoot}/logs/app.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      colorize: true,
    },
    console: {
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true,
    },
    error: {
      level: 'error',
      handleExceptions: true,
      json: true,
      colorize: true,
    }
  };

export const logger: winston.Logger = winston.createLogger({
    transports: [
        new winston.transports.File(options.file),
        new winston.transports.Console(options.console)
    ],
    format: winston.format.combine(
        winston.format.colorize({ colors: {info: 'blue', error: 'red', debug: 'yellow'} }),
        winston.format.simple()
      )
});

// tslint:disable-next-line:no-shadowed-variable
export const morganOption: Options = {
    stream: {
      write (message: string) {
          logger.info(message.trim());
      },
    },
  };
