import * as dotenv from 'dotenv';
// Load file .env
dotenv.config();


export const env = {
    port: process.env.PORT,
    url_mongodb: process.env.URL_MONGODB

}
