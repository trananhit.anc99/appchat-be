FROM node:latest
WORKDIR /home/opt
RUN apt-get update && apt-get install -y vim
COPY ["package.json", "package-lock.json", "./"]
RUN npm install --production
COPY . .
EXPOSE 8090
CMD npm run dev