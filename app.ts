import { env } from './config/default'
import { ServerNode } from './src/core/express';
import { DB } from './src/database/mongoose';
const db = env.url_mongodb;
const startServe = async () => {
   DB({db});
   await ServerNode();
}

startServe();
